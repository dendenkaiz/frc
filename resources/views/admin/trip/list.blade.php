@extends('admin.layout')
<style type="text/css">
    #td1_style    {
        font-family: helvetica; font-size: 17px; font-weight: bold; width: 200px; text-align: center; border: 2px solid #F2A831;
    }
    #table_style  {
        border: 3px solid #F2A831; border-radius: 5px; padding: 3px;
    }
    #input_style2 {
        font-family: helvetica; font-size: 17px; font-weight: bold;  width: 200px; background: #F2A831;
        border-radius: 5px; padding: 3px;
    }
</style>
@section('content')
    <h2>Список подорожей</h2>
    <table id="table_style">
        <th id="td1_style">Клієнт</th>
        <th id="td1_style">Квиток</th>
        <th id="td1_style">Тариф</th>
        <th id="td1_style">Дестинація</th>
        <th id="td1_style">Дія</th>
        @foreach ($trips as $trip)
            <tr>
                <td id="td1_style">
                    <a href="/trips/{{ $trip->trip_id }}">{{ $trip->client}}</a>
                </td>
                <td id="td1_style">{{ $trip->type }}</td>
                <td id="td1_style">{{ $trip->tariff }}</td>
                <td id="td1_style">{{ $trip->destination }}</td>
                <td id="td1_style">
                    <a href="/admin/trips/{{ $trip->trip_id }}/edit">edit</a>
                    <form style="float:right; padding: 0 15px;"
                          action="/admin/trips/{{ $trip->trip_id }}"method="POST">
                        {{ method_field('DELETE') }}

                        {{ csrf_field() }}
                        <button id="input_style2">Delete</button>

                    </form>
                </td>
            </tr>
        @endforeach
    </table>
@endsection
