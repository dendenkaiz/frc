@extends('admin.layout')
<style type="text/css">
    label {
        min-width: 150px;
        display: inline-block;
    }
    #input_style2 {
        font-family: helvetica; font-size: 17px; font-weight: bold;  width: 200px; background: #F2A831;
        border-radius: 5px; padding: 3px;
    }
</style>
@section('content')
    <h2>Редагування подорожі</h2>
    <form action="/admin/trips/{{ $trip->trip_id }}" method="POST">
        {{ method_field('PUT') }}
        {{ csrf_field() }}
        <label>Клієнт</label>
        <input id="input_style2" type="text" name="client" value="{{$trip->client}}">
        <br/><br/>
        <label>Дестинація</label>
        <input id="input_style2" type="text" name="destination" value="{{$trip->destination}}">
        <br/><br/>
        <label>Тариф</label>
        <select id="input_style2" name="tariff">
            @foreach(array('normal', 'VIP') as $tariff_id)
                <option value="{{ $tariff_id }}"
                    {{ ( $tariff_id == $trip->tariff ) ? 'selected' : '' }}>
                    {{ $tariff_id }}
                </option>
            @endforeach
        </select>
        <br/><br/>
        <label>Квиток</label>
        <select id="input_style2" name="type">
            @foreach(array('1 way', '2 way') as $type_id)
                <option value="{{ $type_id }}"
                    {{ ( $type_id == $trip->type ) ? 'selected' : '' }}>
                    {{ $type_id }}
                </option>
            @endforeach
        </select>
        <br/>
        <br/>
        <input id="input_style2" type="submit" value="Зберегти">
        <a id="span_style" href="/admin/trips">Скасувати</a>
    </form>
@endsection
