<html>
<head>
    <title>Інформаційна система "FakeAgency"</title>
</head>
<form id="TCC";>
    <style type="text/css">
        #span_style   {
            font-family: helvetica; font-size: 17px; font-weight: bold; margin: 7px;}
        #main_style1   {
            font-family: helvetica; font-size: 40px; font-weight: bold; margin: 7px; color: #BC3EF6}
        #main_style2   {
            font-family: helvetica; font-size: 40px; font-weight: bold; margin: 7px; color: #F2A831}
    </style>
</form>
<body style="background-color: #D07CF6">
@yield('page_title')
<br/><br/>
<div class="container">
    <span id="span_style">Основна інформація</span>
    @yield('content')
</div>
<br/>
<br/>
<b id="main_style1">Fake</b><b id="main_style2">Agency</b>
</body>
@include('app.layouts.footer')
</html>
