@extends('app.layouts.layouts')
@section('page_title')
    <b id="span_style">Список подорожей</b>
@endsection
@section('content')
    <form id="TCC";>
        <style type="text/css">
            #td1_style    {
                font-family: helvetica; font-size: 17px; font-weight: bold; width: 200px; text-align: center; border: 2px solid #F2A831;
            }
            #span_style   {
                font-family: helvetica; font-size: 17px; font-weight: bold; margin: 7px;
            }
            #table_style  {
                border: 3px solid #F2A831; border-radius: 5px; padding: 3px;
            }
            #input_style2 {
                font-family: helvetica; font-size: 17px; font-weight: bold;  width: 200px; background: #F2A831;
                border-radius: 5px; padding: 3px;
            }
        </style>
    <form method="get" action="/trips">
        <select name="type" id="input_style2">
            <option value="0">всі квитки</option>
            @foreach(array('1 way', '2 way') as $type_id)
                <option value="{{ $type_id }}"
                    {{ ( $type_id == $type_selected ) ? 'selected' : '' }}>
                    {{ $type_id }}
                </option>
            @endforeach
        </select>
        <select name="tariff" id="input_style2">
            <option value="0">всі тарифи</option>
            @foreach(array('normal', 'VIP') as $tariff_id)
                <option value="{{ $tariff_id }}"
                    {{ ( $tariff_id == $tariff_selected ) ? 'selected' : '' }}>
                    {{ $tariff_id }}
                </option>
            @endforeach
        </select>
        <input type="submit" value="Знайти" id="input_style2" />
    </form>
        <table id="table_style">
        <th>Клієнт</th>
        <th>Тариф</th>
        <th>Тип</th>
        <th>Дестинація</th>
        @foreach ($trips as $trip)
            <tr>
                <td id="td1_style">
                    <a href="/trips/{{ $trip->trip_id }}">

                        {{ $trip->client}}
                    </a>
                </td>
                <td id="td1_style">{{ $trip->tariff }}</td>
                <td id="td1_style">{{ $trip->type }}</td>
                <td id="td1_style">{{ $trip->destination }}</td>
            </tr>
        @endforeach
    </table>
    <p id="span_style"><a href="/">На головну сторінку</a></p>
@endsection
