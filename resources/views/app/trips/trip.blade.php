@extends('app.layouts.layouts')
@section('page_title')
    <b>Інформація про поїздку пасажира {{ $trip->client }}</b>
@endsection
@section('content')
    <p>Дестинація - {{ $trip->destination }}</p>
    <p>ID подорожі - {{ $trip->trip_id }}</p>
    <p>Тип подорожі - {{ $trip->type }}</p>
    <p>Тариф - {{ $trip->tariff }}</p>
    <p>Клієнт - {{ $trip->client }}</p>
    <a href="/trips">Дивитися всі подорожі</a>
@endsection
