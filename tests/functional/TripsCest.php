<?php

class TripsCest
{
    public function _before(FunctionalTester $I)
    {
    }
// tests
    public function testTripsInGroup(FunctionalTester $I)
    {
        $I->amOnPage('/trips?type=0&tariff=0');
        $I->canSee('всі квитки', 'option');

    }
// tests
    public function testgetTripByID(FunctionalTester $I)
    {
        $I->HaveInDatabase('trips',
            array(  'type' =>'1 way',
                    'tariff' => 'normal',
                    'client' =>'abraham',
                    'destination' => 'Egypt'));
        $I->seeInDatabase('trips', ['trip_id' => '4']);
    }
}
