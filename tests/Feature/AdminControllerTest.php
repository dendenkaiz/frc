<?php


namespace Tests\Feature;

use App\Http\Controllers\AdminTripController;
use App\Models\User;
use App\Models\Trips;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use phpDocumentor\Reflection\Types\ContextFactory;
use Tests\TestCase;
use Illuminate\Database\Eloquent\Factories\Factory;
use Database\Factories\UserFactory;
use App\Models\Trip;

class AdminControllerTest extends TestCase
{

    /** @test  */

    public function TestLoggedInAdmin(){
        $response = $this->get('/admin/trips')
            ->assertRedirect('/login');
    }

    /** @test  */

    public function TestAuthAdmin(){

        $user = Auth::loginUsingId(1);

        $this->actingAs($user);

        $response = $this->get('/admin/trips')
            ->assertOk();
    }

    /** @test  */

    public function TestAuthUser(){

        $user = Auth::loginUsingId(1);

        $this->actingAs($user);

        $response = $this->get('/admin/trips')
            ->assertOk();
    }

    /** @test  */

    public function TestPost(){
        Event::fake();

        $user = Auth::loginUsingId(2);

        $this->withoutMiddleware();
        $this->actingAs($user);

        $response = $this->post('admin/trips', [
            'type' => '1 way',
            'tariff' => 'normal',
            'client' => 'abraham',
            'destination' => 'Egypt'

        ]);

        $this->assertCount(7, Trip::all());


    }

    // vendor/bin/codecept run --steps --html

}

