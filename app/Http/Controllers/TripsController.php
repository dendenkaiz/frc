<?php

namespace App\Http\Controllers;

use App\Models\Trips;
use Illuminate\Http\Request;

class TripsController extends Controller
{
    public function index(Request $request) {
        $type = $request->input('type', null);
        $tariff = $request->input('tariff', null);
        $model_trips = new Trips();
        $trips = $model_trips->getTrips($type, $tariff);
        return view('app.trips.list', [
                'trips' => $trips,
                'type_selected' => $type,
                'tariff_selected' => $tariff]
        );

    }

    public function trip($id) {

        $model_trips = new Trips();
        $trip = $model_trips->getTripByID($id);
        return view('app.trips.trip')->with('trip', $trip);

    }
}
