<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Trip;
use Illuminate\Support\Facades\Redirect;

class AdminTripController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $trips = Trip::get();
        return view('admin.trip.list', ['trips' => $trips]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.trip.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $client = $request->input('client');
        $tariff = $request->input('tariff');
        $type = $request->input('type');
        $destination = $request->input('destination');
        $trip = new Trip();
        $trip->client = $client;
        $trip->tariff = $tariff;
        $trip->type = $type;
        $trip->destination = $destination;
        $trip->save();
        return Redirect::to('/admin/trips');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $trip = Trip::where('trip_id', $id)->first();
        return view('admin.trip.edit', [
            'trip' => $trip]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $trip = Trip::where('trip_id', $id)->first();
        $trip->client = $request->input('client');
        $trip->tariff = $request->input('tariff');
        $trip->type = $request->input('type');
        $trip->destination = $request->input('destination');
        $trip->save();
        return Redirect::to('/admin/trips');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        Trip::destroy($id);
        return Redirect::to('/admin/trips');
    }
}
