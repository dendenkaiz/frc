<?php

namespace App\Models;
use Illuminate\Support\Facades\DB;


class Trips
{

    public function getTrips($type, $tariff){
        $query = DB::table('trips')
            ->select('trips.trip_id', 'type', 'tariff', 'client', 'destination')
            ->orderBy('trip_id');
        if($type){
            $query->where('type', $type);
        }
        if($tariff){
            $query->where('tariff', $tariff);
        }
        $trips = $query->get();
        return $trips;
    }

    public function getTripByID($id)
    {
        if(!$id) return null;
        $trip = DB::table('trips')
            ->select('trips.trip_id', 'type', 'tariff', 'client', 'destination')
            ->where('trip_id', $id)
            ->get()->first();
        return $trip;
    }
}
